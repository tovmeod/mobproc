import os
from urllib.parse import urlparse

import requests
from celery import Celery

# Set the default Django settings module for the 'celery' program.
from django.db.models import F

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mobproc.settings')

app = Celery('mobproc')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django apps.
app.autodiscover_tasks()


@app.task(bind=True)
def count_words(self, uri):
    # may be a file or an url
    parsed_uri = urlparse(uri)
    scheme = parsed_uri.scheme
    if scheme in ('http', 'https'):
        with requests.get(uri, stream=True) as r:
            r.raise_for_status()
            partial_chunk = None
            for chunk in r.iter_content(chunk_size=8192):
                # If you have chunk encoded response uncomment if
                # and set chunk_size parameter to None.
                #if chunk:
                if partial_chunk:
                    chunk = partial_chunk + chunk
                line = chunk.split()
                for word in line[:-1]:
                    _word_increment(word.decode('utf-8'))
                partial_chunk = line[-1]
            if partial_chunk:
                _word_increment(partial_chunk)
    elif scheme == 'file':
        with open(parsed_uri.netloc, encoding='utf-8') as f:
            for line in f:
                for word in line.split():
                    _word_increment(word)


def _word_increment(word):
    from wordcounter.models import WordCount
    # if this import is on top it fails on django.core.exceptions.AppRegistryNotReady: Apps aren't loaded yet.
    word = word
    obj, created = WordCount.objects.get_or_create(
        word=word,
        defaults={'count': 0},
    )
    obj.count = F("count") + 1
    obj.save(update_fields=["count"])
