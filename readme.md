This document provides a high level system design to the word counter API

### The word counter API service provides 2 endpoints:
1. POST /counter
Use this endpoint to upload data in 3 ways:
   1. File upload
   2. File path to an exiting file on server
   3. URL 
http return codes should be used to indicate success or failure
2. GET /statistics/<word\> 
 
Returns the number of times the word appeared so far or 0.
(It should not return an error in case the word never appeared)

We should consider using the same resource name and accept post or get requests
depending on what is needed instead of using different paths

### Auth
No Authentication, authorization or user management is done on this service,
it is expected to run inside the cluster and not exposed to the outside world,
all requests are expected to come from other services inside the same cluster.
If this service is needed from outside one option is to rely on the existing
user management service and forward requests from there.

### Concurrency
The API server is implemented in django and should be deployed using an application server
like uwsgi (configuration example provided) and a reverse proxy like nginx 
(configuration example provided).

Nginx is responsible for TLS negotiation, socket termination, keep-alive, buffering and 
details on http protocol.

The application server (uwsgi) is responsible for creating a separate python process
for each worker, providing isolation, concurrency, restarting the process after X
requests handled (to minimize the impact of an eventual leak)

If a bigger concurrency is needed (than what one computer may be able to handle),
nginx may be configured to work with multiple uwsgi upstream servers and 
is able to load balance using different strategies. 
(See Choosing a Load-Balancing Method at https://docs.nginx.com/nginx/admin-guide/load-balancer/http-load-balancer/ )

### Implementation considerations
The API is implemented on Django so it can easily evolve to use DRF as needed.

Some approaches were considered on handling the uploaded data:
1. Async - The API server can receive the request and pass to a queue to be handled by async workers.
- The good: The API server may be thinner and be able to handle a higher number of requests and faster, 
this would make the API server an IO bound server.
- The bad: That additional parts needs to be maintained (queue broker/server and queue consumer workers)

3. Upload then process - Naive implementation of http data upload,
nginx buffers the data and pass the request to uwsgi after upload is complete.
- The good: Simple implementation.
- The bad: Keeps the uwsgi worker busy while processing, client waits for the processing to complete
to receive the http response.

3. Process while uploading - stream uploaded data and process in batches
- The good: Overall it could potentially finish processing the data as soon as upload is finished.
- The bad: Not much gained in terms of algorithmic complexity. 
django workers are busy while data is uploaded.
There's a risk of network failure interrupting the upload and client retrying to upload same data, 
meaning the server would be left with a partial processed data result and could potentially
reprocess the same data when client retries. 
No built in support on this on framework, we would need to rely on third party packages
(like https://github.com/jkeifer/drf-chunked-upload) 
(meaning additional time needed to test if it works and risk that it will be abandoned)

Also for implementation 1 and 2 there's a risk of exploding the buffer.

### Some implementation details
With all things considered I decided to implement solution number 1, with the following implementation details:
- Heavy lifting is done by nginx - nginx can be configured to save the file on disk directly 
and pass to the application the file path (using [nginx upload module](https://www.nginx.com/resources/wiki/modules/upload/)):

[ file on client ] -> [ POST to nginx ] --> [ nginx writes to disk ] --> [ pass by reference to uwsgi ]
There's still a need to decide on what to do with the file after processing, delete or keep?
- celery used for async tasks, it can be scaled to use distributed workers.
for this POC it uses the eager strategy, meaning it will run synchronously 

Nginx and uwsgi configuration example provided with the relevant configuration to achieve this
