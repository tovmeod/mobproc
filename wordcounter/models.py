from django.db import models


class WordCount(models.Model):
    max_word_len = 46
    # biggest word is 46 chars pneumoultramicroscopicossilicovulcanoconiótico
    word = models.CharField(unique=True, max_length=max_word_len)
    count = models.IntegerField(default=0)
