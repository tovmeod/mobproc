from urllib.parse import urlencode

from django.test import TestCase, Client


class QuestionModelTests(TestCase):

    def test_statistics_for_new_word(self):
        client = Client()
        response = client.get('/statistics/aloo')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'0')

    def test_send_nothing(self):
        client = Client()
        response = client.post('/counter')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'')

    def test_send_file(self):
        client = Client()
        response = client.post('/counter', urlencode({'file.path': 'testinput.txt'}),
                               content_type="application/x-www-form-urlencoded")
        # use file.path field name to simulate what nginx is supposed to do
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'')
        self.assert_word_count(client, 'new', 0)
        self.assert_word_count(client, 'The', 3)
        self.assert_word_count(client, 'the', 5)
        self.assert_word_count(client, 'is', 0)
        self.assert_word_count(client, 'a', 3)

    def test_send_file_path(self):
        client = Client()
        response = client.post('/counter', urlencode({'filename': 'testinput.txt'}),
                               content_type="application/x-www-form-urlencoded")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'')
        self.assert_word_count(client, 'new', 0)
        self.assert_word_count(client, 'The', 3)
        self.assert_word_count(client, 'the', 5)
        self.assert_word_count(client, 'is', 0)
        self.assert_word_count(client, 'a', 3)

    def test_send_url(self):
        client = Client()
        response = client.post('/counter', urlencode({'url': 'https://pastebin.com/SAADMn9F'}),
                               content_type="application/x-www-form-urlencoded")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'')
        self.assert_word_count(client, 'new', 2)
        self.assert_word_count(client, 'The', 8)
        self.assert_word_count(client, 'the', 13)
        self.assert_word_count(client, 'is', 4)
        self.assert_word_count(client, 'a', 13)

    def assert_word_count(self, client, word, count):
        response = client.get(f'/statistics/{word}')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(int(response.content), count)
