from django.http import HttpResponse, HttpResponseNotAllowed
from django.views.decorators.csrf import csrf_exempt

from mobpoc.celery import count_words
from wordcounter.models import WordCount


@csrf_exempt
def counter(request):
    if request.method != "POST":
        return HttpResponseNotAllowed(['POST'])
    # get file name or url and send to async worker
    # file uploaded is passed as a file name from nginx
    path = request.POST.get("file.path")
    if path:
        count_words.delay(f'file://{path}')
    else:
        filename = request.POST.get('filename')
        if filename:
            # do we want to check the file exists here?
            count_words.delay(f'file://{filename}')
        else:
            url = request.POST.get('url')
            if url:
                # do we want to validate the url here? we could validate the format
                # but we would still need to validate the url exists, so we can do all validation on worker
                count_words.delay(url)
    return HttpResponse()


def statistics(request, word):
    if request.method != "GET":
        return HttpResponseNotAllowed(['GET'])
    try:
        count = WordCount.objects.get(word=word).count
    except WordCount.DoesNotExist:
        count = 0
    return HttpResponse(str(count))


def send_file():
    pass